//Fonction permettant de faire apparaitre et disparaitre le menu laterale en fonction de la position sur la page


window.addEventListener('scroll', function(){

	scrollValue = (window.innerHeight + window.scrollY) / (document.body.offsetHeight);

	console.log(scrollValue);

	if(scrollValue > 0.33){
		document.getElementById('menu').style.opacity = '1';
	}
	if(scrollValue < 0.33){
		document.getElementById('menu').style.opacity = '0';
	}
})


//Fonctions pour le menu laterale, permet de changer l'image au survol

function onimageCunong(){
	this.setAttribute("src","./Image/CunnongBlanc.png");
	document.getElementById("entreprise").style.border="0.15rem solid";
	document.getElementById("entreprise").style.borderRadius="50%"
}

function outimageCunong(){
	this.setAttribute("src","./Image/CunnongNoir.png");
	document.getElementById("entreprise").style.border="none"
}

function onimageActeurs(){
	this.setAttribute("src","./Image/LogoActeurEco.png");
	document.getElementById("actor").style.border="0.15rem solid";
	document.getElementById("actor").style.borderRadius="50%"
}

function outimageActeurs(){
	this.setAttribute("src","./Image/LogoActeurEco2.png");
	document.getElementById("actor").style.border="none"
}

function onimageSWOT(){
	this.setAttribute("src","./Image/LogoSWOT2.png");
	document.getElementById("SwOt").style.border="0.15rem solid";
	document.getElementById("SwOt").style.borderRadius="50%"
}

function outimageSWOT(){
	this.setAttribute("src","./Image/LogoSWOT.png");
	document.getElementById("SwOt").style.border="none"
}

function onimageFoot(){
	this.setAttribute("src","./Image/LogoFootprint2.png");
	document.getElementById("foot").style.border="0.15rem solid";
	document.getElementById("foot").style.borderRadius="50%"
}

function outimageFoot(){
	this.setAttribute("src","./Image/LogoFootprint.png");
	document.getElementById("foot").style.border="none"
}
